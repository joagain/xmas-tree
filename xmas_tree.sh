#!/bin/bash

trap "tput reset; tput cnorm; exit" 2
clear
tput civis
offset=5 # TODO: make it configurable
line=$offset
COLUMNS=$(tput cols) 
half=$((COLUMNS / 2))
tput cup $line 0;

n=20 # TODO: make it configurable
#TODO: error if 2n-1 greater than width of term
#TODO: error if height is more than n+offset-1
# Don't forget height of the message too
for ((i=1;i<=n;i++))
do

    #TODO: refactor

    tput setaf 7; tput bold;
    for ((k=0;k<=half-1-n;k++))
    do
        echo -ne ❄;
    done

    for ((k=i;k<=n;k++))
    do
        echo -ne ❄;
    done

    tput setaf 2; tput bold;
    for ((j=1;j<=2*i-1;j++))
    do
        echo -ne \^
    done

    tput setaf 7; tput bold;
    for ((k=0;k<=half-1-n;k++))
    do
        echo -ne ❄;
    done

    for ((k=i;k<n;k++))
    do
        echo -ne ❄;
    done

    #This echo used for printing new line
    echo;

    line=$((line + 1))
done

tput sgr0; tput setaf 3

tput cup $line 0
title="Hello world!" 
printf "%*s\n" $(((${#title}+$COLUMNS)/2)) "$title"
mpg123 -q Deck_the_Halls_USAFB.mp3 < /dev/null &

while true; do
    color=$((RANDOM % 9)) # 8 colors + 9th for decoration
    line=$((RANDOM % n))
    column=$((RANDOM % (line+1)))
    sign=$((RANDOM % 2))
    # ternary
    [[ $sign = 0 ]] && column=$((half-column)) || column=$((half+column))
    tput cup $((line+offset)) $column
    if [ $color -ne 8 ] ; then
        tput setaf $color; tput bold
        echo -ne \*
    else
        echo -ne 🎁
    fi
    sleep 0.02
done